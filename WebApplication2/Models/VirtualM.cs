﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class VirtualM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IPPublic { get; set; }
        [Required]
        public string Login { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
