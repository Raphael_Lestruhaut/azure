﻿using Azure.Core;
using Azure.Identity;
using Azure.ResourceManager.Compute.Models;
using Azure.ResourceManager.Compute;
using Azure.ResourceManager.Network.Models;
using Azure.ResourceManager.Network;
using Azure.ResourceManager.Resources;
using Azure.ResourceManager;
using Azure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Data;
using WebApplication2.Models;
using System.Security.Claims;

namespace WebApplication2.Controllers
{
    /// <summary>
    /// Contrôleur qui gère les actions autour des VMs
    /// </summary>
    public class VirtualMsController : Controller
    {
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Groupe de ressources où sont stocké les VMs ainsi que les adresse ip, les réseaux virtuels et les interfaces liés
        /// </summary>
        private ResourceGroupResource GroupeRessource = null;

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        /// <param name="context"></param>
        public VirtualMsController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Récupère la liste des VMs
        /// </summary>
        /// <returns>Retourne une vue qui affiche la liste des VMs</returns>
        public async Task<IActionResult> Index()
        {
            await this.getRessourceGroupe();
            if(User.FindFirstValue(ClaimTypes.Name) != null)
            {
                var allVms = await _context.VirtualM.ToListAsync();
                var userVms = allVms.Where(vm => vm.Name == "VM" + getUserName());

                return _context.VirtualM != null
                    ? View(userVms)
                    : Problem("Entity set 'ApplicationDbContext.VirtualM'  is null.");
            }
            else
            {
                return View(new List<VirtualM>());
            }
            
        }

        /// <summary>
        /// Récupère les détails d'une vm précise
        /// </summary>
        /// <param name="id">id de la VM à afficher</param>
        /// <returns>Retourne une vue qui affiche le détail d'une VM</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.VirtualM == null)
            {
                return NotFound();
            }

            var virtualM = await _context.VirtualM
                .FirstOrDefaultAsync(m => m.Id == id);
            if (virtualM == null)
            {
                return NotFound();
            }

            return View(virtualM);
        }

        /// <summary>
        /// Affiche la vue qui permet de créer une VM
        /// </summary>
        /// <returns>Retourne la vue qui permet de créer une VM</returns>
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Méthode qui créer une VM dans la base de données et dans Azure
        /// </summary>
        /// <param name="virtualM">Data pour créer la VM</param>
        /// <returns>Renvoie la vue qui affiche la liste des VM</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Login,Password")] VirtualM virtualM)
        {
            string currentUserId = this.getUserName();

            //création groupe ressource si nécessaire
            if (this.GroupeRessource == null)
            {
                this.GroupeRessource = await this.getRessourceGroupe();
            }

            //création de l'ip
            PublicIPAddressResource ip = await this.createIP(currentUserId);
            //création VN
            VirtualNetworkResource vn = await this.createVN(currentUserId);
            //création network interface
            NetworkInterfaceResource nc = await this.createNC(currentUserId, vn, ip);
            //création VM
            await this.createVM(currentUserId, virtualM.Login,virtualM.Password, ip, vn, nc);

            //récupération de l'ip une fois affecté à la VM
            PublicIPAddressResource ipRecup = this.InitIp(currentUserId, this.GroupeRessource);

            //nom vm
            string nomVM = "VM" + currentUserId;

            //maj modèle
            ModelState.Remove(nameof(virtualM.IPPublic));
            ModelState.Remove(nameof(virtualM.Name));
            virtualM.Name = nomVM;
            virtualM.IPPublic = ipRecup.Data.IPAddress;
            if (ModelState.IsValid)
            {
                _context.Add(virtualM);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(virtualM);
        }

        /// <summary>
        /// Fonction qui permet d'éditer une VM
        /// </summary>
        /// <param name="id"Id de la machine a éditer</param>
        /// <returns>Retourne la vue qui permet d'éditer une VM</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.VirtualM == null)
            {
                return NotFound();
            }

            var virtualM = await _context.VirtualM.FindAsync(id);
            if (virtualM == null)
            {
                return NotFound();
            }
            return View(virtualM);
        }

        /// <summary>
        /// Fonction qui update la machine en BDD
        /// </summary>
        /// <param name="id">Id de la machine a éditer</param>
        /// <param name="virtualM">Data de la machine virtuelle</param>
        /// <returns>La vue qui affiche la liste des VMs</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,IPPublic,Login,Password")] VirtualM virtualM)
        {
            if (id != virtualM.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(virtualM);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VirtualMExists(virtualM.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(virtualM);
        }

        /// <summary>
        /// Affiche la vue qui permet de delete une machine
        /// </summary>
        /// <param name="id">Id de la machine à delete</param>
        /// <returns>La vue qui affiche les détails de la machine ainsi que les boutons retour et delete</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.VirtualM == null)
            {
                return NotFound();
            }

            var virtualM = await _context.VirtualM
                .FirstOrDefaultAsync(m => m.Id == id);
            if (virtualM == null)
            {
                return NotFound();
            }

            return View(virtualM);
        }

        /// <summary>
        /// Supprimer la VM dans la base de données
        /// </summary>
        /// <param name="id">Id de la VM a supprimer</param>
        /// <returns>La vue qui contient la liste des VMs</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.VirtualM == null)
            {
                return Problem("Entity set 'ApplicationDbContext.VirtualM'  is null.");
            }

            var vmModel = await _context.VirtualM.FindAsync(id);
            if (vmModel != null)
            {
                _context.VirtualM.Remove(vmModel);

                if(this.GroupeRessource == null)
                {
                    this.GroupeRessource = await this.getRessourceGroupe();
                }
                string idGen = vmModel.Name.Substring(2);

                VirtualMachineResource vm = await this.GroupeRessource.GetVirtualMachines().GetAsync("VM" + idGen);
                NetworkInterfaceResource nic =
                    await this.GroupeRessource.GetNetworkInterfaces().GetAsync("NC" + idGen);
                VirtualNetworkResource vn = await this.GroupeRessource.GetVirtualNetworks().GetAsync("VN" + idGen);
                PublicIPAddressResource publicIp =
                    await this.GroupeRessource.GetPublicIPAddresses().GetAsync("IP" + idGen);

                await vm.DeleteAsync(WaitUntil.Completed, forceDeletion: true);
                await nic.DeleteAsync(WaitUntil.Completed);
                await vn.DeleteAsync(WaitUntil.Completed);
                await publicIp.DeleteAsync(WaitUntil.Completed);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Fonction qui permet d'éteindre une machine si elle allumé, ou d'allumer une machine si elle est éteinte
        /// </summary>
        /// <param name="nom">Nom de la VMs sur laquelle il faut agir</param>
        /// <returns>La vue qui affiche la liste des VMs</returns>
        public async Task<IActionResult> Shutdown(string nom)
        {
            if(this.GroupeRessource== null) 
            {
                this.GroupeRessource = await this.getRessourceGroupe();
            }

            VirtualMachineCollection vms = this.GroupeRessource.GetVirtualMachines();

            foreach (VirtualMachineResource vm in vms)
            {
                if(nom.Equals(vm.Id.Name))
                {
                    if((await statutVM(nom)))
                    {
                        await vm.PowerOnAsync(WaitUntil.Completed);
                    }
                    else
                    {
                        await vm.PowerOffAsync(WaitUntil.Completed);
                    }           
                }
            }
            return RedirectToAction(nameof(Index));
        }

        /// <summary>
        /// Fonction qui permet de savoir si une VM est allumée
        /// </summary>
        /// <param name="nom">Nom de la VM dont on veut savoir le statut</param>
        /// <returns>Retourne vrai si la VM est éteinte sinon false</returns>
        public static async Task<bool> statutVM(string nom)
        {
            bool resultat = false;
            ArmClient armClient = new(new ClientSecretCredential("b7b023b8-7c32-4c02-92a6-c8cdaa1d189c", "bd360398-35fa-45c0-bcc8-8b52eeffef65", "_Am8Q~S0MX8AFXUY89PainwqgCysmbIooJf4bbt2"));
            SubscriptionResource subscription = armClient.GetDefaultSubscription();
            ResourceGroupResource resourceGroupResource = armClient.GetDefaultSubscription().GetResourceGroup("rg-machineClient-001");


            VirtualMachineCollection vms = resourceGroupResource.GetVirtualMachines();

            foreach (VirtualMachineResource vm in vms)
            {
                if (nom.Equals(vm.Id.Name))
                {

                   resultat = vm.InstanceView(CancellationToken.None).Value.Statuses[1].Code.Equals("PowerState/stopped");
                }
            }
            return resultat;
        }

        /// <summary>
        /// Permet de savoir si une VM existe dans la base de données
        /// </summary>
        /// <param name="id">Id de la VM dont on veut s'assurer de l'existance</param>
        /// <returns>returne vrai si la VM existe sinon faux</returns>
        private bool VirtualMExists(int id)
        {
          return (_context.VirtualM?.Any(e => e.Id == id)).GetValueOrDefault();
        }

        /// <summary>
        /// Fonction qui créer une VM dans azure
        /// </summary>
        /// <param name="idUser">Nom de l'utilisateur qui créer la VM</param>
        /// <param name="login">Login choisis par l'utilisateur</param>
        /// <param name="password">Password choisis par l'utilisateur</param>
        /// <param name="ipResource">Ip a affecter à la machine virtuelle</param>
        /// <param name="vnetResrouce">Réseau de la machine virtuelle</param>
        /// <param name="nicResource">Network interface de la VM</param>
        /// <returns></returns>
        private async Task<bool> createVM(string idUser, string login, string password, PublicIPAddressResource ipResource, VirtualNetworkResource vnetResrouce, NetworkInterfaceResource nicResource)
        {
            string nomVM = "VM" + idUser;
            ArmClient client = new(new ClientSecretCredential("b7b023b8-7c32-4c02-92a6-c8cdaa1d189c", "bd360398-35fa-45c0-bcc8-8b52eeffef65", "_Am8Q~S0MX8AFXUY89PainwqgCysmbIooJf4bbt2"));
            SubscriptionResource subscription = await client.GetDefaultSubscriptionAsync();
            ResourceGroupCollection resourceGroups = subscription.GetResourceGroups();

            //création vm
            VirtualMachineCollection vms = this.GroupeRessource.GetVirtualMachines();

            VirtualMachineResource vmResource = vms.CreateOrUpdate(
            WaitUntil.Completed,
            nomVM,
            new VirtualMachineData(AzureLocation.WestEurope)
            {
                HardwareProfile = new VirtualMachineHardwareProfile()
                {
                    VmSize = VirtualMachineSizeType.StandardB1S
                },
                OSProfile = new VirtualMachineOSProfile()
                {
                    ComputerName = "computeName",
                    AdminUsername = login,
                    AdminPassword = password,
                    LinuxConfiguration = new LinuxConfiguration()
                    {
                        DisablePasswordAuthentication = false,
                        ProvisionVmAgent = true
                    }
                },
                StorageProfile = new VirtualMachineStorageProfile()
                {
                    OSDisk = new VirtualMachineOSDisk(DiskCreateOptionType.FromImage)
                    {
                        DeleteOption = DiskDeleteOptionType.Delete,
                    },
                    ImageReference = new ImageReference()
                    {
                        Offer = "UbuntuServer",
                        Publisher = "Canonical",
                        Sku = "18.04-LTS",
                        Version = "latest"
                    }
                },
                NetworkProfile = new VirtualMachineNetworkProfile()
                {
                    NetworkInterfaces =
                    {
                        new VirtualMachineNetworkInterfaceReference()
                        {
                            Id = nicResource.Id
                        }
                    }
                },
            }).Value;

            return true;
        }
        /// <summary>
        /// Fonction qui créer un groupe ressource
        /// </summary>
        /// <returns>retourne le groupe ressources créer</returns>
        private async Task<ResourceGroupResource> createRessourceGroup()
        {

            ArmClient client = new(new ClientSecretCredential("b7b023b8-7c32-4c02-92a6-c8cdaa1d189c", "bd360398-35fa-45c0-bcc8-8b52eeffef65", "_Am8Q~S0MX8AFXUY89PainwqgCysmbIooJf4bbt2"));
            SubscriptionResource subscription = await client.GetDefaultSubscriptionAsync();
            ResourceGroupCollection resourceGroups = subscription.GetResourceGroups();

            string resourceGroupName = "rg-machineClient-001";
            AzureLocation location = AzureLocation.WestEurope;
            ResourceGroupData resourceGroupData = new ResourceGroupData(location);
            ArmOperation<ResourceGroupResource> operation = await resourceGroups.CreateOrUpdateAsync(WaitUntil.Completed, resourceGroupName, resourceGroupData);
            ResourceGroupResource resourceGroup = operation.Value;

            return resourceGroup;
        }

        /// <summary>
        /// Fonction qui créer une adresse ip dans azure
        /// </summary>
        /// <param name="Login">Pseudo du compte pour qui on créer l'adresse ip</param>
        /// <returns>l'adresse ip créer</returns>
        private async Task<PublicIPAddressResource> createIP(string Login)
        {
            string nomIp = "IP"+Login;
            PublicIPAddressCollection publicIps = this.GroupeRessource.GetPublicIPAddresses();

            //first step create a public ip
            PublicIPAddressResource ipResource = publicIps.CreateOrUpdate(
            WaitUntil.Completed,
            nomIp,
            new PublicIPAddressData()
            {
                PublicIPAddressVersion = NetworkIPVersion.IPv4,
                PublicIPAllocationMethod = NetworkIPAllocationMethod.Dynamic,
                Location = AzureLocation.WestEurope
            }).Value;

            return ipResource;
        }

        /// <summary>
        /// Fonction qui créer un virtual network dans azure
        /// </summary>
        /// <param name="login">Pseudo du compte pour qui on créer le virtual network</param>
        /// <returns>Le virtual network créer</returns>
        private async Task<VirtualNetworkResource> createVN(string login)
        {
            string nameVN = "VN" + login;

            VirtualNetworkCollection vns = this.GroupeRessource.GetVirtualNetworks();

            //second step virtual network
            VirtualNetworkResource vnetResrouce = vns.CreateOrUpdate(
            WaitUntil.Completed,
            nameVN,
            new VirtualNetworkData()
            {
                Location = AzureLocation.WestEurope,
                Subnets =
                {
                    new SubnetData()
                    {
                        Name = "testSubNet",
                        AddressPrefix = "10.0.0.0/24"
                    }
                },
                AddressPrefixes =
                {
                    "10.0.0.0/16"
                },
            }).Value;

            return vnetResrouce;
        }

        /// <summary>
        /// Fonction qui créer une network interface
        /// </summary>
        /// <param name="login">Pseudo du compte pour qui on créer la network interface</param>
        /// <param name="vnetResrouce">Virtual Network de l'interface</param>
        /// <param name="ipResource">Ip de l'interface</param>
        /// <returns>La network interface créer</returns>
        private async Task<NetworkInterfaceResource> createNC(string login, VirtualNetworkResource vnetResrouce, PublicIPAddressResource ipResource)
        {
            string ncName = "NC" + login;
            NetworkInterfaceCollection nics = this.GroupeRessource.GetNetworkInterfaces();

            //third step interface
            NetworkInterfaceResource nicResource = nics.CreateOrUpdate(
            WaitUntil.Completed,
            ncName,
            new NetworkInterfaceData()
            {
                Location = AzureLocation.WestEurope,
                IPConfigurations =
                {
                    new NetworkInterfaceIPConfigurationData()
                    {
                        Name = "Primary",
                        Primary = true,
                        Subnet = new SubnetData() { Id = vnetResrouce?.Data.Subnets.First().Id },
                        PrivateIPAllocationMethod = NetworkIPAllocationMethod.Dynamic,
                        PublicIPAddress = new PublicIPAddressData() { Id = ipResource?.Data.Id }
                    }
                }
            }).Value;

            return nicResource;
        }

        /// <summary>
        /// Fonction qui permet de récupérer l'adresse ip d'une VM
        /// </summary>
        /// <param name="login">Login de la personne qui a créer la VM</param>
        /// <param name="resourceGroup">Ressource groupe où se trouve la VM</param>
        /// <returns>Retourne l'adresse ip</returns>
        private PublicIPAddressResource InitIp(string login, ResourceGroupResource resourceGroup)
        {
            string ipName = "IP" + login;
            var publicIps = resourceGroup.GetPublicIPAddresses();
            var ipResource = publicIps.CreateOrUpdate(
                WaitUntil.Completed,
                ipName,
                new PublicIPAddressData()
                {
                    PublicIPAddressVersion = NetworkIPVersion.IPv4,
                    PublicIPAllocationMethod = NetworkIPAllocationMethod.Dynamic,
                    Location = AzureLocation.WestEurope
                }).Value;

            return ipResource;
        }

        /// <summary>
        /// recupère le groupe de ressources
        /// </summary>
        /// <returns>le groupe de ressources des VMs</returns>
        private async Task<ResourceGroupResource> getRessourceGroupe()
        {
            ResourceGroupResource resultat = null;
            //on try de récupérer le ressources groupe déjà existant
            try
            {
                ArmClient armClient = new(new ClientSecretCredential("b7b023b8-7c32-4c02-92a6-c8cdaa1d189c", "bd360398-35fa-45c0-bcc8-8b52eeffef65", "_Am8Q~S0MX8AFXUY89PainwqgCysmbIooJf4bbt2"));
                resultat = armClient.GetDefaultSubscription().GetResourceGroup("rg-machineClient-001");
            }
            catch(Azure.RequestFailedException ex)
            {
                //si la requête échoue avec ce code d'erreur c'est que le groupe existe pas donc on le créer   
                if(ex.ErrorCode.Equals("ResourceGroupNotFound"))
                {
                    resultat = await this.createRessourceGroup();
                }
            }
            return resultat;
        }

        /// <summary>
        /// Fonction qui récupère le user name
        /// </summary>
        /// <returns>le user name</returns>
        /// <exception cref="NullReferenceException">Exceptiojn qui est lancé si l'user n'est pas connecté</exception>
        private string getUserName()
        {
            //récupération login
            string? currentUserId = User.FindFirstValue(ClaimTypes.Name);
            if (currentUserId != null)
            {
                currentUserId = currentUserId.Split("@")[0];
                currentUserId = currentUserId.Replace(".", "");
            }
            else
            {
                throw new NullReferenceException();
            }

            return currentUserId;
        }
    }
}
