# Valeurs des variables

# Resource Group
rg_name     = "rg-gaming-666"
rg_location = "North Europe"

# App Service Plan 
asp_name     = "asp-gaming-666"
asp_sku_name = "B1"

# Windows Web App
app_name       = "raphaelplay"
dotnet_version = "v7.0"

# DB
db_server_name  = "vm-raphael"
db_login        = "RaphaelDeus"
db_password     = "4-v3ry-53cr37-p455w0rd"
db_account_name = "raphael"
db_name         = "vm-databasev2"