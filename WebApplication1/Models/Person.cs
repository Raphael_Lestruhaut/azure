﻿using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace WebApplication1.Models
{
    public class Person
    {
        /// <summary>
        /// Représente l'id de la personne
        /// </summary>
        [Required]
        [Range(1,100)]
        public string Id { get; set; }
        /// <summary>
        /// Représente le prénom de la personne
        /// </summary>
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string FirstName { get; set; }
        /// <summary>
        /// Représente le nom de la personne
        /// </summary>
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string LastName { get; set; }
        /// <summary>
        /// Représente l'email de la personne
        /// </summary>
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        /// <param name="Id">Id de la personne</param>
        /// <param name="FirstName">Prénom de la personne</param>
        /// <param name="LastName">Nom de la personne</param>
        /// <param name="Email">Email de la personne</param>
        public Person(string Id, string FirstName, string LastName, string Email)
        {
            this.Id = Id;
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Email = Email;
        }
    }
}
